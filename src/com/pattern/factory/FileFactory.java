package com.pattern.factory;

public class FileFactory {

	FileCreator fileCreator;

	public FileCreator getInstance(String fileType) {

		switch (fileType) {
		case "csv":
			fileCreator = new CsvFileCreator();
			break;
		case "excel":
			fileCreator = new ExcelFileCreator();
			break;

		default:
			break;
		}
		return fileCreator;
	}

}
