package com.pattern.prototype;

import java.util.ArrayList;
import java.util.List;

public class JarLibrary implements Cloneable{
	
	private String jarName ;
	
	private List<Jar> jars=new ArrayList<Jar>();

	public String getJarName() {
		return jarName;
	}

	public void setJarName(String jarName) {
		this.jarName = jarName;
	}

	public List<Jar> getJars() {
		return jars;
	}

	public void setJars(List<Jar> jars) {
		this.jars = jars;
	}
	
	public List<Jar> loadJars() {
		// could be a db opertn
		for (int i = 0; i < 10; i++) {
			Jar jar = new Jar();
			jar.setVersion("Jar_" + i);
			jar.setSize(i + 10);
			getJars().add(jar);
		}
		return this.jars;

	}

	@Override
	public String toString() {
		return "JarLibrary [jarName=" + jarName + ", jars=" + jars + ", getJarName()=" + getJarName() + ", getJars()="
				+ getJars() + ", loadJars()=" + loadJars() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	@Override
	protected JarLibrary clone() throws CloneNotSupportedException {
		JarLibrary clonedObj=new JarLibrary();
		clonedObj.setJars(fetchJarsFromObj(getJars()));

		return clonedObj;
		}

	private List<Jar> fetchJarsFromObj(List<Jar> jars) {
		List<Jar> deepCloningJars = new ArrayList<Jar>();
		jars.forEach(jar -> {
			deepCloningJars.add(jar);
		});
		return deepCloningJars;
	}

}
