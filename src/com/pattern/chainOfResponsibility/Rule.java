package com.pattern.chainOfResponsibility;

public interface Rule {

	public String canGetAdmission(Applicant applicant);

}
