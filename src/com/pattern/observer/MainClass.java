package com.pattern.observer;

public class MainClass {

	// has a list of subscribers and all will be notified once we get a mail#
	// typical example is JMS topic

	public static void main(String[] args) {

		JMSTopic topic = new JMSTopic("Hey message queued for last 2 days");
		LocalQueueSubscriber subscriber = new LocalQueueSubscriber();
		topic.addSubscription(subscriber); // subscribed
		topic.messageRecieved();
	}
}
