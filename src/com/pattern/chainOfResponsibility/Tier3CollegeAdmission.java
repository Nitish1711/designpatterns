/**
 * 
 */
package com.pattern.chainOfResponsibility;

/**
 * @author nitkulsh
 *
 */
public class Tier3CollegeAdmission implements Rule {

 
	@Override
	public String canGetAdmission(Applicant applicant) {
		if (applicant.getScore().getRank() <= 50000 || applicant.getScore().getPercentile() < 80)
			return "Congrats"+applicant.getName()+" . You are eligible for Tier3 college admission";
		else
			return "Please try again next year!";

	}

}
