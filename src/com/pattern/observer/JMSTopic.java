package com.pattern.observer;

import java.util.ArrayList;
import java.util.List;

public class JMSTopic implements Subject {

	public List<Observer> observers;

	private String message;

	public JMSTopic(String message) {
		super();
		this.message = message;
		this.observers = new ArrayList<Observer>();
	}

	@Override
	public void addSubscription(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeSubscription(Observer o) {
		observers.remove(o);

	}

	@Override
	public void notifySubscribers() {
		observers.forEach(observer -> {
			observer.update(this.message);
		});
	}

	public void messageRecieved() {
		notifySubscribers();
	}

}
