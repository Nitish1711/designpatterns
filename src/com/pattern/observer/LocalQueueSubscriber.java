package com.pattern.observer;

public class LocalQueueSubscriber implements Observer {

	@Override
	public void update(String message) {

		System.out.println(message);
	}

}
