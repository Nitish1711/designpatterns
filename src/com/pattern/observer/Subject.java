package com.pattern.observer;

public interface Subject {
	
	public void addSubscription(Observer o);
	
	public void removeSubscription(Observer o);

	public void notifySubscribers();


}
