package com.pattern.prototype;

public class Jar {
	
	private String version ;
	private int size;
	
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	@Override
	public String toString() {
		return "Jar [version=" + version + ", size=" + size + ", getVersion()=" + getVersion() + ", getSize()="
				+ getSize() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
