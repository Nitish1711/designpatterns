package com.pattern.decorator;

public class Trainer extends TennisDecorator { // these are called the decorators

	private CourtBooking cb;

	private int bookingCost = 400;

	public Trainer(CourtBooking cb) {
		this.cb = cb;
	}

	@Override
	public int getCost() {
		return (bookingCost + cb.getCost());
	}

}
