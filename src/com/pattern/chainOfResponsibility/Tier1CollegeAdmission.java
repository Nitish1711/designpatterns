/**
 * 
 */
package com.pattern.chainOfResponsibility;

/**
 * @author nitkulsh
 *
 */
public class Tier1CollegeAdmission implements Rule {

	private Rule next;

	public Tier1CollegeAdmission() {
		next = new Tier2CollegeAdmission();
	}

	@Override
	public String canGetAdmission(Applicant applicant) {
		if (applicant.getScore().getRank() <= 100)
			return "Congrats"+applicant.getName()+" . You are eligible for Tier1 college admission";
		else
			return next.canGetAdmission(applicant);

	}

}
