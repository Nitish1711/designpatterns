package com.pattern.factory;

public class CsvFileCreator implements FileCreator {

	@Override
	public void readThroughFile() {
		System.out.println("Reading from the csv file");

	}

	@Override
	public void writeInFile() {
		System.out.println("Write to csv file");
	}

}
