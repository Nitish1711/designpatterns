package com.pattern.chainOfResponsibility;

public class Applicant {
	
	private int id;
	private String name;
	private Score score;
	/**
	 * @param id
	 * @param name
	 * @param score
	 */
	public Applicant(int id, String name, Score score) {
		super();
		this.id = id;
		this.name = name;
		this.score = score;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the score
	 */
	public Score getScore() {
		return score;
	}
	

}
