/**
 * 
 */
package com.pattern.factory;

/**
 * @author Dell
 *
 */
public interface FileCreator {
	
	public void readThroughFile();
	
	public void writeInFile();

}
