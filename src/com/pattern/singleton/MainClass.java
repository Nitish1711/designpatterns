package com.pattern.singleton;

public class MainClass {

	// generally used for caching and logging

	public static void main(String[] args) {

		Singleton instance = Singleton.getInstance();
		System.out.println(instance);
	}
}
