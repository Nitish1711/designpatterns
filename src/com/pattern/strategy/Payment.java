package com.pattern.strategy;

public interface Payment {  //this whole with a implementation serves as a set of algorithm and strategy 

	public String getAccountNumber();
}
