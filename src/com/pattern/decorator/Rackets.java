package com.pattern.decorator;

public class Rackets extends TennisDecorator {  //these are called the decorators 

	private CourtBooking cb;   

	private int bookingCost = 200;

	public Rackets(CourtBooking cb) {
		this.cb = cb;
	}

	@Override
	public int getCost() {
		return (bookingCost + cb.getCost());
	}

}
