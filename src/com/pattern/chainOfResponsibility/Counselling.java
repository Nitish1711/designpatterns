/**
 * 
 */
package com.pattern.chainOfResponsibility;

/**
 * @author nitkulsh
 *
 */
public class Counselling {
	
	public static void main(String[] args) {
		Applicant applicant1=new Applicant(1, "Rahul", new Score(99, 50L));
		Applicant applicant2=new Applicant(1, "Ramkesh", new Score(70, 45678L));
		
		Rule rule=new Tier1CollegeAdmission();
		
		System.out.println(rule.canGetAdmission(applicant1));
		System.out.println(rule.canGetAdmission(applicant2));


	}

}
