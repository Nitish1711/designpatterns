/**
 * 
 */
package com.pattern.builder;

/**
 * @author nitkulsh
 *
 */
public class BouquetShop {

	public static void main(String[] args) {
		Bouquet customBouquet = Bouquet.builder().withjasmine("jasmine").withlavender("lavender").withrose("Rose").build();
		Bouquet lilyBouqet = Bouquet.builder().withLily("lily").build();
		System.out.println(customBouquet);
		System.out.println(lilyBouqet);

	}

}
