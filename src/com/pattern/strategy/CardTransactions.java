package com.pattern.strategy;

public class CardTransactions implements Payment {
	
	private String accountNumber;
	
	public CardTransactions(String accountNumber) {
		this.accountNumber=accountNumber;
	}

	@Override
	public String getAccountNumber() {
		System.out.println("I am  providing u account number"+accountNumber);
		return accountNumber;
	}

}
