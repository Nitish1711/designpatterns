package com.pattern.chainOfResponsibility;

public class Score {
	
	private int percentile;
	private Long rank;
	/**
	 * @return the percentile
	 */
	public int getPercentile() {
		return percentile;
	}
	/**
	 * @param percentile
	 * @param rank
	 */
	public Score(int percentile, Long rank) {
		super();
		this.percentile = percentile;
		this.rank = rank;
	}
	/**
	 * @return the rank
	 */
	public Long getRank() {
		return rank;
	} 

}
