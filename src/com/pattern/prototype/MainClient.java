package com.pattern.prototype;

public class MainClient {

	// requires whenever creating an object is costiler or takes more time . say a
	// db opertn where values are first fetch from db and then set to the object

	// say we are picking each and every jar from database or downloading from
	// internet

	public static void main(String[] args) {

		JarLibrary jarLibrary = new JarLibrary();
		jarLibrary.setJarName("Connectors");
		jarLibrary.loadJars(); // created obj , expensive one

		JarLibrary jarLibrary2 = null;
		try {
			jarLibrary2 = jarLibrary.clone();
			jarLibrary2.setJarName("Web containers");
			System.out.println("Library1 " + jarLibrary);
			System.out.println("Library2 " + jarLibrary2);

		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
