package com.pattern.strategy;

public class CustomerMainClass {
	
	private PaymentDetails mode;
	
	public  void makePayment() {
		//if card payment
		mode=new CardPayment("12344");
		mode.makePayment();
	}
	
	public static void main(String[] args) {
		CustomerMainClass obj=new CustomerMainClass();
		obj.makePayment();
	}

}
