package com.pattern.singleton;

public class Singleton {

	private static volatile Singleton instance = null; // as member is shared amongst the threads

	private Singleton() {
	}

	public static Singleton getInstance() {
		if (instance == null) {
			synchronized (Singleton.class) {  //double check locking 
				if (instance == null) {
					instance = new Singleton();
				}
			}
		}
		return instance;
	}

}
