/**
 * 
 */
package com.pattern.chainOfResponsibility;

/**
 * @author nitkulsh
 *
 */
public class Tier2CollegeAdmission implements Rule{
	
	private Rule next;
	
	public Tier2CollegeAdmission() {
		next=new Tier3CollegeAdmission();
	}

	@Override
	public String canGetAdmission(Applicant applicant) {
		if (applicant.getScore().getRank() <= 1000)
			return "Congrats"+applicant.getName()+" . You are eligible for Tier2 college admission";
		else
			return next.canGetAdmission(applicant);

	}

}
