package com.pattern.observer;

public interface Observer {

	public void update(String message);
}
