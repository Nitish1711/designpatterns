package com.pattern.decorator;

public class GrassCourtBooking extends CourtBooking {// these are called decorated objects

	@Override
	public int getCost() {
		return 300;
	}

}
