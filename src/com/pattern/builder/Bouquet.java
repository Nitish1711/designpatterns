/**
 * 
 */
package com.pattern.builder;

/**
 * @author nitkulsh
 *
 */

public class Bouquet {

	private String lily;

	private String rose;

	private String lavender;

	private String jasmine;

	private String lime;

	public static Builder builder() {
		return new Builder();
	}

	static class Builder {

		private String lily;

		private String rose;

		private String lavender;

		private String jasmine;

		private String lime;

		public Builder withLily(String lily) {
			this.lily = lily;
			return this;
		}

		public Builder withlime(String lime) {
			this.lime = lime;
			return this;
		}

		public Builder withlavender(String lavender) {
			this.lavender = lavender;
			return this;
		}

		public Builder withrose(String rose) {
			this.rose = rose;
			return this;
		}

		public Builder withjasmine(final String jasmine) {
			this.jasmine = jasmine;
			return this;
		}

		public Bouquet build() {
			Bouquet bouquet = new Bouquet();
			bouquet.jasmine = jasmine;
			bouquet.rose = rose;
			bouquet.lavender = lavender;
			bouquet.lime = lime;
			bouquet.lily = lily;
			return bouquet;
		}

	}

	@Override
	public String toString() {
		return "Bouquet [lily=" + lily + ", rose=" + rose + ", lavender=" + lavender + ", jasmine=" + jasmine
				+ ", lime=" + lime + "]";
	}

}