package com.pattern.decorator;

public class MainClass {

	public static void main(String[] args) {

		// say if someone wants to book the grass court for tennis . the cost would be

		CourtBooking cbCourt = new GrassCourtBooking();
		CourtBooking dc1 = new Rackets(cbCourt);//decorating our court 
		CourtBooking dc2 = new BallBox(dc1);
		CourtBooking dc3 = new Trainer(dc2);

		System.out.println("Total cost would be " + dc3.getCost());

	}

}
