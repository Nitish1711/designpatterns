package com.pattern.strategy;

public class CardPayment extends PaymentDetails{
	
	private String accntNumber;
	
	public CardPayment(String accntNumber) {
		this.accntNumber=accntNumber;
	}

	@Override
	public void makePayment() {
		payment=new CardTransactions(accntNumber);
		payment.getAccountNumber();
		
	}
	
	

}
