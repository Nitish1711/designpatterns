package com.pattern.strategy;

public class CashPayment extends PaymentDetails {

	public CashPayment() {
	}

	@Override
	public void makePayment() {
		payment = new CashTransactions();

	}

}
