package com.pattern.factory;

public class FactoryClient {
	
	//creational design pattern .
	//hiding object creation from the client .
	
	public static void main(String[] args) {
		String fileType="csv";
		FileCreator creator=new FileFactory().getInstance(fileType);
		creator.readThroughFile();		
	}

}
