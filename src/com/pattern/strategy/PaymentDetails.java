package com.pattern.strategy;

public abstract class PaymentDetails {
	
	private int amount ;
	
	public Payment payment;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public abstract void makePayment() ;

	
	/*
	 * public String getAccountNumber() { // if i put it here then cashpayment aslo
	 * gets this one which is of no use and // voilateing solid of interfcae
	 * seggregation. Also overrrdiding this method in // every class will increasae
	 * code duplicacy return "2ss"; }
	 */
}
