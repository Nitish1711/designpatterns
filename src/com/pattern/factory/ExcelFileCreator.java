package com.pattern.factory;

public class ExcelFileCreator implements FileCreator {

	@Override
	public void readThroughFile() {
		System.out.println("reading through the excel file");
	}

	@Override
	public void writeInFile() {
		System.out.println("Writing to the excel file");

	}

}
